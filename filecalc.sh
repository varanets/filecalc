#!/bin/bash

MAX=true; FILEOUT=false; RESULTOUT=true; 
#ALL=false

function option {
if [[ "$1" == *"m"* ]]; then MAX=false; fi;
if [[ "$1" == *"M"* ]]; then MAX=true; fi;
if [[ "$1" == *"f"* ]]; then FILEOUT=false; fi;
if [[ "$1" == *"F"* ]]; then FILEOUT=true; fi;
if [[ "$1" == *"r"* ]]; then RESULTOUT=false; fi;
if [[ "$1" == *"R"* ]]; then RESULTOUT=true; fi;
#if [[ "$1" == *"S"* ]]; then ALL=true; fi;
if [ "$FILEOUT" == "false" -a "$RESULTOUT" == "false" ];
then echo "Предупреждение! Вы отказались от вывода на экран!" >&2; fi;
}

case $1 in
	'--help') cat README.md; exit 0;;
	-*) option $1; shift;;
esac

if [[ $# == 0 ]]; then echo "Ошибка! Ни один файл не задан!" >&2; exit 1; fi;
FILE=false; RESULT=false
#declare -a RESULT
for i in $@
do
err=0;
formula=$(cat $i) || err=1;
if [[ "$err" == "1" ]]; then echo "Предупреждение! Файл $i не может быть прочитан!"; continue; fi;
if [[ "$formula" == *[a-zA-Z]* ]]; then err=2; fi;
result=$(($formula)) || err=2;
if [[ "$err" == "2" ]]; then echo "Предупреждение! Файл $i содержит неверную формулу!"; continue; fi;
#k=$((${#RESULT[@]}/2))
if [[ "$RESULT"  == "false" ]]; then RESULT=$result; FILE=$i; continue; fi
if [[ "$RESULT" -eq "$result" ]]; then FILE=$FILE' '$i; fi
if [[ "$MAX" == "true" && "$result" -gt "$RESULT" ]]; then FILE=$i; RESULT=$result; fi
if [[ "$MAX" == "false" && "$result" -lt "$RESULT" ]]; then FILE=$i; RESULT=$result; fi
done;

if [[ "$RESULT" == "false" ]]; then echo "Ошибка! Ни один не может быть посчитан!" >&2; exit 1; fi;
if [[ "$FILEOUT" == "true" ]]; then echo $FILE; fi
if [[ "$RESULTOUT" == "true" ]]; then echo $RESULT; fi
exit 0
